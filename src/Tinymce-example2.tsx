import React from 'react';
import { Editor as TinyMCEEditor } from '@tinymce/tinymce-react';
// import { tinymce } from 'tinymce';

class TinymceExample2 extends React.Component<any, any> {
  private editor;

  constructor(props) {
    super(props);
    this.state = { showButton: false };
  }

  render() {
    console.log('render', this.state.showButton);
    return (
      <div className="tinymce-example2">
        <div className="tinymce-example2-editor-wrapper">
          <div>
            {/*<textarea className="hola-div" />*/}
            <TinyMCEEditor
              // id="holaeditor1"
              onFocus={(e) => {
                console.log('focus', e);
                this.setState({ showButton: true });
              }}
              onBlur={(e) => {
                console.log('blur', e);
                this.setState({ showButton: false });
                return false;
              }}
              onEditorChange={(value, editor) => {
                console.log(value);
                console.log(editor.annotator);
                console.log(editor.contentCSS);
              }}
              apiKey="5u65pjso056apg0tjkh8105r7sfxgx70y8wd49a12e5npscf"
              init={{
                // selector: 'div.content-div',
                // menubar: false,
                plugins: [
                  'advlist autolink lists link image charmap print preview anchor',
                  'searchreplace visualblocks code fullscreen',
                  'insertdatetime media table paste code help wordcount emoticons',
                ],
                // menu: {
                //   a: { title: 'yo', items: 'blockformats' },
                // },
                // menubar: 'a',
                menubar: false,
                // block_formats:
                //   'Paragraph=h3; Heading 1=h2; Heading 2=h1; Heading 3=h3; Heading 4=h4; Heading 5=h5; Heading 6=h6; Preformatted=pre',
                toolbar:
                  'bold italic underline strikethrough blockquote | fontselect fontsizeselect forecolor backcolor | link unlink | emoticons | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat',
                // branding: false,
                // elementpath: false,
                init_instance_callback: (editor) => {
                  console.log('init_instance_callback', editor);
                  // editor.focus();
                  editor.fire('focus');
                  // editor.execCommand('FontName', false, 'Arial');
                },
                setup: (editor) => {
                  this.editor = editor;
                  editor.on('blur', () => {
                    console.log('blurrrr');
                    this.setState({ showButton: false });
                    return false;
                  });
                  console.log('here');
                  // editor.ui.registry.addButton('mysidebar', {
                  //   text: 'btn',
                  //   onAction: function () {
                  //     // Open window
                  //     editor.insertContent('hola there');
                  //   },
                  // });
                },
                // custom_ui_selector: 'span#holaid-custom',
                inline: true,
                // fixed_toolbar_container: '.fixed-toolbar-container',
                skin: 'naked',
              }}
            />
          </div>
          <div className="content-div-wrapper">
            <div className="content-div" />
          </div>
          <div className="toolbar-wrapper">
            <div className="fixed-toolbar-container" />
            <div style={{ marginLeft: '10px' }}>
              <button
                onClick={() => {
                  console.log(this.editor);
                  this.editor.insertContent('{{hola}}');
                }}
              >
                +
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default TinymceExample2;
