import React from 'react';
import { Editor } from '@tinymce/tinymce-react';

class TinymceExample extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = { value: '<p>initial value</p>' };
  }

  render() {
    return (
      <div className="editor-wrapper-wrapper">
        <div className="editor-wrapper">
          {/*<textarea id="testid" />*/}
          <Editor
            apiKey="5u65pjso056apg0tjkh8105r7sfxgx70y8wd49a12e5npscf"
            // cloudChannel="5-dev"
            value={this.state.value}
            init={{
              // selector: 'textarea#testid',
              branding: false,
              height: '100%',
              menubar: false,
              plugins: [
                'advlist',
                'autolink',
                'lists',
                'link',
                'image',
                'charmap',
                'print',
                'preview',
                'anchor',
                'searchreplace',
                'visualblocks',
                'code',
                'fullscreen',
                'insertdatetime',
                'media',
                'table',
                'paste',
                'code',
                'help',
                'wordcount',
                'emoticons',
                'fullscreen',
              ],
              contextmenu: 'link image table',
              // toolbar: [
              //   'fullscreen undo redo',
              //   'formatselect',
              //   'bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help emoticons',
              // ],
              // toolbar: [
              //   'fullscreen undo redo formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help emoticons',
              // ],
              toolbar: [
                // 'undo redo ' +
                'bold italic | ' +
                  'alignleft aligncenter alignright alignjustify | ' +
                  'bullist numlist outdent indent | ' +
                  'removeformat | ' +
                  // 'help ' +
                  'emoticons | ' +
                  'fullscreen | customtoolbaroption1',
              ],
              preview_styles:
                'font-family font-size font-weight font-style text-decoration text-transform color background-color border border-radius outline text-shadow',
              statusbar: false,
              // inline: true,
              setup: (editor: any) => {
                editor.ui.registry.addButton('customtoolbaroption1', {
                  text: 'hello',
                  // icon: false,
                  tooltip: 'helloooo',
                  onAction: (a: any) => {
                    console.log(a.isDisabled());
                    console.log(a.setDisabled());
                  },
                });
              },
              custom_ui_selector: '.customButton',
              // content_style: 'body { background: red }',
              inline: true,
              fixed_toolbar_container: '#toolbarContainer',
              resize: false,
            }}
            onEditorChange={(value) => {
              console.log(value);
              this.setState({ value });
            }}
            onInit={(...args) => {
              // console.log(args[0].target.focus());
              // console.log(args[1].focus());
            }}
            onBlur={(...args) => {
              console.log(args);
            }}
          />
          <div
            id="toolbarContainer"
            style={{
              border: '1px solid black',
              position: 'absolute',
              bottom: '5px',
              left: '5px',
            }}
          />
          {/*<button className="customButton">Hola amigos</button>*/}
        </div>
      </div>
    );
  }
}

export default TinymceExample;
