import React from 'react';
import TinymceExample2 from './Tinymce-example2';
// import DraftExample from './Draft-example';
// import DraftWysiwyg from './Draft-wysiwyg';

const App = () => {
  return <TinymceExample2 />;
  // return <TinymceExample />;
  // return <DraftExample />;
  // return <DraftWysiwyg />;
};

export default App;
